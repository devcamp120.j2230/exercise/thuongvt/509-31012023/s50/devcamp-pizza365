
// khai báo thư viện mongoose
const mogoose = require("mongoose");

// khai báo class schema của mongo
const schema = mogoose.Schema;

//khai báo  review schema 
const orderSchema = new schema({
    // trường id
    _idOder: {
        type: mogoose.Types.ObjectId,
        unique: true
    },
    // trường orderCode
    orderCode:{
        type: String,
        unique: true,
    },
    // trường pizzaSize
    pizzaSize:{
        type: String,
        required:true
    },
    // trường pizzaType
    pizzaType:{
        type: String,
        required: true
    },
    // trường voucher
    voucher: {
        type: mogoose.Types.ObjectId,
        ref:"Voucher"
    },
     // trường drink
     drink: {
        type: mogoose.Types.ObjectId,
        ref:"Drink"
    },
    // trường status
    status:{
        type: String,
        required: true
    },
    // trường ngày tạo
    ngayTao:{
        type: Date,
        default: Date.now()
    },

    // trường ngày cập nhật
    ngayCapNhat:{
        type: Date,
        default: Date.now()
    }
})

module.exports = mogoose.model("Order", orderSchema);
