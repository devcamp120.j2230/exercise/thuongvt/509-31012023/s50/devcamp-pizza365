const getAllUserMiddleware = (req,res,next)=>{ 
    console.log("get all User");
    next()
}

const getAUserMiddleware = (req,res,next)=>{
    console.log("get a User");
    next();
}

const postUserMiddleware = (req,res,next)=>{
    console.log("post a User");
    next();
}

const putUserMiddleware = (req,res,next)=>{
    console.log("put a User");
    next();
}

const deleteUserMiddleware = (req,res,next)=>{
    console.log("delete a User");
    next();
}

module.exports = {
    getAllUserMiddleware,
    getAUserMiddleware,
    putUserMiddleware,
    postUserMiddleware,
    deleteUserMiddleware
}
