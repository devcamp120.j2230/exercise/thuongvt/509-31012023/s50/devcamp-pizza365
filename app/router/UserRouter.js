//Khai báo thư viện express
const express = require("express");

//Khai báo Middleware 
const {
    getAllUserMiddleware,
    getAUserMiddleware,
    putUserMiddleware,
    postUserMiddleware,
    deleteUserMiddleware
} = require(`../middleware/UserMiddleware`);

//Tạo rouer 
const UserRouter = express.Router();

//Sử dụng router
UserRouter.get("/User",getAllUserMiddleware,(req,res)=>{
    res.json({
        message: "Get all User"
    })
});

UserRouter.get("/User/:UserId",getAUserMiddleware,(req,res)=>{
    let UserId = req.params.UserId
    res.json({
        message: `UserId = ${UserId}`
    })
});

UserRouter.post("/User",postUserMiddleware,(req,res)=>{
    res.json({
        message: "Get all User"
    })
});

UserRouter.put("/User/:UserId",putUserMiddleware,(req,res)=>{
    let UserId = req.params.UserId
    res.json({
        message: `UserId = ${UserId}`
    })
});

UserRouter.delete("/User/:UserId",deleteUserMiddleware,(req,res)=>{
    let UserId = req.params.UserId
    res.json({
        message: `UserId = ${UserId}`
    })
});

module.exports = {UserRouter};
