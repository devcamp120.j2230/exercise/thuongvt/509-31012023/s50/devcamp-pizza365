//Khai báo thư viện express
const express = require("express");

//Khai báo middleware 
const {
    getAllOrderMiddleware,
    getAOrderMiddleware,
    postOrderMiddleware,
    putOrderMiddleware,
    deleteOrerMiddleware
    } = require(`../middleware/OrderMiddleware`);

//Tạo router
const OrderRouter = express.Router();

//Sử dụng Router
OrderRouter.get("/Order",getAllOrderMiddleware,(req,res)=>{
    res.json({
        message:"Get all Order"
    })
});
OrderRouter.get("/Order/:OrderId",getAOrderMiddleware,(req,res)=>{
    let OrderId = req.params.OrderId;
    res.json({
        message:`OrderId = ${OrderId}`
    })
});
OrderRouter.post("/Order",postOrderMiddleware,(req,res)=>{
    res.json({
        message:"Post a Order"
    })
});
OrderRouter.put("/Order/:OrderId",putOrderMiddleware,(req,res)=>{
    let OrderId = req.params.OrderId;
    res.json({
        message:`OrderId = ${OrderId}`
    })
});
OrderRouter.delete("/Order/:OrderId",deleteOrerMiddleware,(req,res)=>{
    let OrderId = req.params.OrderId;
    res.json({
        message:`OrderId = ${OrderId}`
    })
});

module.exports = {OrderRouter}
