/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// khai báo đối tượng chọn combo
var gSelectedMenu = {
    menuName: "", 
    duongKinhCM: 0,
    suongNuong: 0,
    saladGr: 0,
    drink: 0,
    priceVND: 0,
  };
// khai báo một đối tượng chưa Type pizaa được chọn
var gSelectedPizzaType = "";
// định nghĩa biến toán cục để lưu thông tin khách hàng
var gOrder = 
{
  menuCombo: null,
  loaiPizza: "",
  hoVaTen: "",
  email: "",
  dienThoai: "",
  diaChi: "",
  loiNhan: "",
  voucher:"",
  loainuocuong:""
};
// Khai báo biến toàn cục để lưu phần trăm giảm giá của voucher
var gPercent = 0;
 // Khai báo một biến toàn cục để lưu OderId của khách hàng
 var gOderId = "";


$(document).ready(function(){

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

// API lấy danh sách nước uống từ server 
onGetDrinkList()

// hàm được gọi khi ấn nút chọn kích cỡ s
$("#btn-small").on("click",function(){
    changeButtonColor("small");// hàm đổi màu nút bấm 
    var vSelectedMenu = getComboSelected("S",20,2,200,2,150000);// lấy dứa liệu đổ dữ liệu vào một biến toàn cục
    vSelectedMenu.displayInConsoleLog(); // ghi dữ liệu ra console
    gSelectedMenu = vSelectedMenu; // lưu trữ dữ liệu vào biến toàn cục
});

// hàm được gọi khi ấn nút chọn kích cỡ l
$("#btn-medium").on("click",function(){
    changeButtonColor("medium");// hàm đổi màu nút bấm 
    var vSelectedMenu = getComboSelected("M",25,4,300,3,200000);// lấy dứa liệu đổ dữ liệu vào một biến toàn cục
    vSelectedMenu.displayInConsoleLog();// ghi dữ liệu ra console
    gSelectedMenu = vSelectedMenu; // lưu trữ dữ liệu vào biến toàn cục
});

// hàm được gọi khi ấn nút chọn kích cỡ m
$("#btn-large").on("click",function(){
    changeButtonColor("large");// hàm đổi màu nút bấm 
    var vSelectedMenu = getComboSelected("M",30,8,500,4,250000);// lấy dứa liệu đổ dữ liệu vào một biến toàn cục
    vSelectedMenu.displayInConsoleLog();// ghi dữ liệu ra console
    gSelectedMenu = vSelectedMenu; // lưu trữ dữ liệu vào biến toàn cục
});

// hàm được gọi khi ấn nút chọn vị pizza hải sản
$("#btn-haisan").on("click",function(){
    gSelectedPizzaType = "Hải sản";
    changeButtonTypeColor(gSelectedPizzaType);
    console.log(gSelectedPizzaType);
})

// hàm được gọi khi ấn nút chọn vị pizza ha wai
$("#btn-hawai").on("click",function(){
    gSelectedPizzaType = "Ha Wai";
    changeButtonTypeColor(gSelectedPizzaType);
    console.log(gSelectedPizzaType);
})

// hàm được gọi khi ấn nút chọn vị pizza gà 
$("#btn-chicken").on("click",function(){
    gSelectedPizzaType = "Chicken";
    changeButtonTypeColor(gSelectedPizzaType);
    console.log(gSelectedPizzaType);
})

// Hàm được ấn khi click vào nút gửi đơn
$(".btn-send").on("click",function(){
    // B1 thu thập thông tin khách hàng
    gOrder = getDataOjectOder()
    // B2 Kiểm tra thông tin thu thập được
    var vCheck = checkValidatedOject(gOrder);
    console.log(vCheck);
    console.log(gOrder);
    if(vCheck){
        loadDataInModal(gOrder);
        $("#user-modal").modal('show');
    }
})

// Hàm được ấn khi ấn nút tạo đơn hàng
$("#btn-modal-add").on("click", function(){
  onBtnTaoDonClick()
})

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

// hàm đổi màu nút bấm chọn thông tin pizza
function changeButtonColor(paramColor){
    //console.log("đã ấn nút")
    if(paramColor==="small"){
      "use strict";
        $("#btn-small").css({"color":"white","background-color":"yellow"});
        $("#btn-medium").css({"color":"white","background-color":"#ff9800"});
        $("#btn-large").css({"color":"white","background-color":"#ff9800"});
    }
    if(paramColor==="medium"){
        $("#btn-small").css({"color":"white","background-color":"#ff9800"});
        $("#btn-medium").css({"color":"white","background-color":"yellow"});
        $("#btn-large").css({"color":"white","background-color":"#ff9800"});
    }
    if(paramColor==="large"){
        $("#btn-small").css({"color":"white","background-color":"#ff9800"});
        $("#btn-medium").css({"color":"white","background-color":"#ff9800"});
        $("#btn-large").css({"color":"white","background-color":"yellow"});
    }
}

// hàm đổi amuf nút bấm chọn loại Pizza
function changeButtonTypeColor(paramType){
  "use strict";
    if(paramType==="Hải sản"){
        $("#btn-haisan").css({"color":"white","background-color":"#20cc11"});
        $("#btn-hawai").css({"color":"white","background-color":"#ff5722"});
        $("#btn-chicken").css({"color":"white","background-color":"#ff5722"});
    }
    if(paramType==="Ha Wai"){
        $("#btn-haisan").css({"color":"white","background-color":"#ff5722"});
        $("#btn-hawai").css({"color":"white","background-color":"#20cc11"});
        $("#btn-chicken").css({"color":"white","background-color":"#ff5722"});
    }
    if(paramType==="Chicken"){
        $("#btn-haisan").css({"color":"white","background-color":"#ff5722"});
        $("#btn-hawai").css({"color":"white","background-color":"#ff5722"});
        $("#btn-chicken").css({"color":"white","background-color":"#20cc11"});
    }
}

// hàm lấy thông tin trên menu và được tham số hoá
    function getComboSelected(paramMenuName,paramDuongKinhCM,paramSuongNuong,paramSaladGr,paramDrink,paramPriceVND) {
        var vSelectedMenuChosse = {
          menuName: paramMenuName, // S, M, L
          duongKinhCM: paramDuongKinhCM,
          suongNuong: paramSuongNuong,
          saladGr: paramSaladGr,
          drink: paramDrink,
          priceVND: paramPriceVND,
          displayInConsoleLog() {
            console.log("Menu được chọn là ");
            console.log(this.menuName);
            console.log("duongKinhCM: " + this.duongKinhCM);
            console.log("suongNuong: " + this.suongNuong);
            console.log("saladGr: " + this.saladGr);
            console.log("drink:" + this.drink);
            console.log("priceVND: " + this.priceVND);
          },
        };
        return vSelectedMenuChosse;
      }

    // hàm lấy dánh sách đồ uống từ server
    function onGetDrinkList(){
      "use strict";
        $.ajax({
            url: "/drinks",
            type: "GET",
            dataType: 'json',
            success: function(responseObject){
              //console.log(responseObject)
              loadListDrinkOnPage(responseObject);
            },
            error: function(error){
              console.assert(error.responseText);
            }
          })
        }

    // hàm load danh dách đồ uống lên page
    function loadListDrinkOnPage(paramData){
      console.log(paramData.drink);
      "use strict";
        var vSelectDrinkElement = $("#select-drink"); // truy vấn vào ô load dink
        for (var bDrinkIndex = 0; bDrinkIndex < paramData.drink.length; bDrinkIndex++) {
            $("<option/>",{
            "value":paramData.drink[bDrinkIndex].maNuocUong,
            "text":paramData.drink[bDrinkIndex].tenNuocUong 
             }).appendTo(vSelectDrinkElement);
          }
    }

    // hàm thu thập thông tin đối tượng oder
    function getDataOjectOder(){
      "use strict";
      // goi ham tinh phan tram
      getVoucher($(".inp-voucher").val().trim());

    var vCustomerInfo = {
            menuCombo: gSelectedMenu,
            loaiPizza: gSelectedPizzaType,
            hoVaTen: $(".inp-name").val().trim(),
            email: $(".inp-email").val().trim(),
            dienThoai: $(".inp-phone").val().trim(),
            diaChi: $(".inp-address").val().trim(),
            loiNhan: $(".inp-message").val().trim(),
            voucher: $(".inp-voucher").val().trim(),
            loainuocuong: $("#select-drink option:selected").val(),
            priceAnnualVND: function () {  // số tiền phải thanh toán
              var vTotal = this.menuCombo.priceVND *(1 - gPercent/ 100);
              return vTotal;
            }
          };
          return vCustomerInfo;
       }
    // Hàm kiểm tra thông tin nhập vào
    function checkValidatedOject(paramData){
      "use strict";
        if (paramData.menuCombo.menuName === "") {
            alert(" Bạn chưa chọn Menu Pizza");
            return false;
          }
          if(paramData.loaiPizza ===""){
            alert(" Bạn chưa chọn loại Pizza");
            return false;
          }
          if (paramData.hoVaTen === "") {
            alert(" Bạn chưa nhập Họ và Tên");
            return false;
          }
          var vEmailHopLe = kiemTraEmail(paramData.email)
          if (vEmailHopLe) {
            return true;
         }
    
         var vSoDienThoaiHople = kiemTraSoDienThoai(paramData.dienThoai)
          if (vSoDienThoaiHople){
            return true;
         }
          if (paramData.diaChi === "") {
            alert(" Bạn chưa nhập địa chỉ");
            return false;
          }
          return true;
       }

    // Hàm kiểm tra số điện thoại
    function kiemTraSoDienThoai(paramSoDienThoai){
      "use strict";
        "use strict";
        if (paramSoDienThoai==""){
          alert("Bạn chưa nhập số điện thoại");
          return false;
      }
        if(!Number(paramSoDienThoai)) {
          alert("Số nhập vào phải là số");
          return false;
      }
          return true;
      }

    // hàm kiểm tra Email
    function kiemTraEmail (paramEmail) {
        "use strict";
       if (paramEmail.trim()==""){
          alert("Bạn chưa nhập Email");
       return false
       }
        if(paramEmail.trim().includes("@")==false) {
          alert ("Email nhập vào chưa đúng")
        return false
        }
        else {
          var vPostAcCong = paramEmail.trim().indexOf("@");
          if(vPostAcCong== 0 || vPostAcCong==paramEmail.trim().length - 1) {
           alert("Email nhập vào chưa đúng");
          return false;
          }
        }
        return true
    }
    
    // hàm kiểm tra mã giảm giá
    function getVoucher(paramData){
      "use strict";
        $.ajax({
            url: "/devcamp-voucher-api/voucher_detail/"+ paramData ,
            type: "GET",
            async:false,
            dataType: 'json',
            success: function(ResVoucher){
              console.log("có mã giảm giá, mã giảm giá là:",ResVoucher)
              gPercent = ResVoucher.phanTramGiamGia;
              return true;
            },
            error: function(error){
            console.log(" không có mã giảm giá", error) 
            }
        })
    }

    // hàm load thông tin lên modal
    function loadDataInModal(paramData){
      "use strict";
        $("#id-ho-ten").val(paramData.hoVaTen);
        $("#id-dien-thoai").val(paramData.dienThoai);
        $("#id-dia-chi").val(paramData.diaChi);
        $("#inp-message").val(paramData.loiNhan);
        $("#id-ma-giam-gia").val(paramData.voucher);
        $("#id-chi-tiet").val("Xác nhận Họ và Tên : " + paramData.hoVaTen +" "
        + ",Địa chỉ : " + paramData.diaChi +" "
        + ",Điện thoại :" + paramData.dienThoai + " "
        + ",Menu combo :" + paramData.menuCombo.menuName + " "
        + ",Đường Kính : "+ paramData.menuCombo.duongKinhCM  + " "
        + ",Sườn nướng :"+ paramData.menuCombo.suongNuong  + " "
        + ",nước uống : "+ paramData.menuCombo.drink + " "
        + ",Loại pizza : " + paramData.loaiPizza + " "
        + ",Giá :" + paramData.menuCombo.priceVND + " "
        +",Mã Giảm Giá :" + paramData.voucher + " "
        + ",Phải thanh toán :" + paramData.priceAnnualVND() + " "
        +",phần trăm giảm giá : " + gPercent +"%")  + " "
        +",Lời nhắn :" + paramData.loiNhan;
    }

    // Hàm được gọi khi ấn nút tạo đơn
    function onBtnTaoDonClick(){
      "use strict";
      //B0 Khai báo một biến để chứa dữ liệu
      var vUser = {
        kichCo:"",
        duongKinh:"",
        suon: "",
        salad: "",
        loaiPizza: "",
        idVourcher: "",
        idLoaiNuocUong: "",
        soLuongNuoc: "",
        hoTen:"",
        thanhTien:"",
        email:"",
        soDienThoai:"",
        diaChi:"",
        loiNhan:"",
      }
    //B1 Thu thập dữ liệu
    getDataUser(vUser); // hàm thu thập dữ liệu
    // B2 Kiểm tra dữ liệu (không cần kiêm tra)
    //B3 Ghi dữ liệu đơn hàng vào server
    ghiUserVaoSever(vUser);
    }

    //hầm lấy dữ liệu cho nút gửi đơn
    function getDataUser(paraDataUser){
        "use strict";
        paraDataUser.kichCo = gOrder.menuCombo.menuName;
        paraDataUser.duongKinh = gOrder.menuCombo.duongKinhCM;
        paraDataUser.suon = gOrder.menuCombo.suongNuong;
        paraDataUser.salad = gOrder.menuCombo.saladGr;
        paraDataUser.loaiPizza = gOrder.loaiPizza;
        paraDataUser.idVourcher = gOrder.voucher;
        paraDataUser.idLoaiNuocUong = gOrder.loainuocuong;
        paraDataUser.soLuongNuoc = gOrder.menuCombo.drink;
        paraDataUser.hoTen = gOrder.hoVaTen;
        paraDataUser.thanhTien = gOrder.menuCombo.priceVND;
        paraDataUser.email = gOrder.email;
        paraDataUser.diaChi = gOrder.diaChi;
        paraDataUser.soDienThoai = gOrder.dienThoai;
        paraDataUser.loiNhan = gOrder.loiNhan;
      }

      // Hàm ghi dữ liệu lên sever
      function ghiUserVaoSever(paramData){
        $.ajax({
          async:false,
          url:"/devcamp-pizza365/orders",
          type:"POST",
          contentType:"application/json",
          data:JSON.stringify(paramData),
          success: function(paramsuccess){
            console.log("thêm user thành công");
             gOderId = paramsuccess.orderId;
             console.log(gOderId)
             $("#user-modal").modal('hide');
             $("#id-goderid").val(gOderId);
             $("#user-modal-orderId").modal('show');
          },
          error: function(paramErr){
              console.log(paramErr.responseText);
          }
         })
      }
      /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
})



